﻿#undef UNICODE
#include <windows.h>
#include <stdio.h>
#include <tchar.h>

#define BUFSIZE 4096

DWORD WINAPI InstanceThread(LPVOID);
VOID ProcessRequest(LPCSTR, LPCSTR, LPDWORD);

int main(VOID)
{
What is Lorem Ipsum?
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

Why do we use it?
It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).



	for (;;)
	{
		hPipe = CreateNamedPipe(
			lpszPipename, // nume pipe
			PIPE_ACCESS_DUPLEX, // acces read/write
			PIPE_TYPE_MESSAGE | // pipe de tip mesaj
			PIPE_READMODE_MESSAGE | // mod de citire mesaj
			PIPE_WAIT, // mod blocant
			PIPE_UNLIMITED_INSTANCES, // nr. max. de instante
			BUFSIZE, // dimensiune buffer output
			BUFSIZE, // dimensiune buffer input
			0, // client time-out
			NULL); // atribut de securitate implicit
		if (hPipe == INVALID_HANDLE_VALUE)
		{
			printf("CreatePipe a esuat!");
			return 0;
		}
		// Asteapta conectarea unui client; daca se termina cu succes,
		// funcţia returneaza o valoare nenula. Daca funcţia returneaza
		// 0, atunci GetLastError returneaza ERROR_PIPE_CONNECTED daca
		// clientul este conectat
		fConnected = ConnectNamedPipe(hPipe, NULL) ?
			TRUE : (GetLastError() == ERROR_PIPE_CONNECTED);
		if (fConnected)
		{
			// Creeaza un fir pentru clientul respectiv
			hThread = CreateThread(
				NULL, // fara atribute de securitate
				0, // dimensiune stiva implicita
				InstanceThread, // funcţia firului de executie
				(LPVOID)hPipe, // parametru pentru fir
				0, // nu este suspendat dupa creare
				&dwThreadId); // thread ID
			if (hThread == NULL)
			{
				printf("CreateThread failed");
				return 0;
			}
			else CloseHandle(hThread);
		}
		else
			// Clientul nu se poate conecta, deci inchidem pipe-ul
			CloseHandle(hPipe);
	}
	return 1;
}
DWORD WINAPI InstanceThread(LPVOID lpvParam)
{
	TCHAR chRequest[BUFSIZE];
	TCHAR chReply[BUFSIZE];
	DWORD cbBytesRead, cbReplyBytes, cbWritten;
	BOOL fSuccess;
	HANDLE hPipe;
	// Firul receptioneaza ca parametru un handle al unei instante a
	// pipe-ului

	hPipe = (HANDLE)lpvParam;
	while (1)
	{
		// Citeste mesaj de la client
		fSuccess = ReadFile(
			hPipe, // handle al pipe-ului
			chRequest, // buffer de receptie date
			BUFSIZE * sizeof(TCHAR), // dimensiune buffer
			&cbBytesRead, // numar de octeti cititi
			NULL); // op de I/O care nu este suprapusa (overlapped)
			// (cu alte cuvinte, este sincrona)
		if (!fSuccess || cbBytesRead == 0) break;
		ProcessRequest(chRequest, chReply, &cbReplyBytes);
		// Scrie raspunsul in pipe
		fSuccess = WriteFile(
			hPipe, // handle al pipe-ului
			chReply, // buffer din care se transmit datele
			cbReplyBytes, // numar de octeti de scris
			&cbWritten, // numar de octeti transmisi efectiv
			NULL); // op de I/O care nu este suprapusa (overlapped)
			// (cu alte cuvinte, este sincrona)
		if (!fSuccess || cbReplyBytes != cbWritten) break;
	}
	// Goleste buffer-ul pipe-ului (flush) pentru a permite clientului
	// sa citeasca continutul acestuia inaintea deconectarii.
	// Apoi deconecteaza pipe-ul şi inchide handle-ul instantei
	// acestuia.
	FlushFileBuffers(hPipe);
	DisconnectNamedPipe(hPipe);
	CloseHandle(hPipe);
	return 1;
}
VOID ProcessRequest(LPTSTR chRequest, LPTSTR chReply, LPDWORD pchBytes)
{
	char message[255] = "Numerele primite sunt: ";
	strcat(message, chRequest);
	printf("%s\n", message);

	int sum = 0;
	char* numbers = strtok(chRequest, ",");

	while (numbers != NULL)
	{
		sum = sum + atoi(numbers);
		numbers = strtok(NULL, ",");
	}

	char* buffer = malloc(sizeof(char) + 1);
	_itoa(sum, buffer, 10);

	char response[255] = "Suma numerelor primite este: ";
	strcat(response, buffer);
	 
	// depune in chReply raspunsul de la server
	strncpy_s(chReply, BUFSIZE, TEXT(response), BUFSIZE);
	// depune la adresa pchBytes nr. de octeti ai mesajului de raspuns
	*pchBytes = (lstrlen(chReply) + 1) * sizeof(TCHAR);
}