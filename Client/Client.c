#undef UNICODE
#include <windows.h>  
#include <stdio.h> 
#include <conio.h> 
#include <tchar.h> 

#define BUFSIZE 512   
int main(int argc, TCHAR* argv[])
{
	HANDLE hPipe;
	LPCSTR lpvMessage = TEXT("Mesaj de la client!");
	TCHAR chBuf[BUFSIZE];
	BOOL fSuccess;
	DWORD cbRead, cbWritten, dwMode;
	LPCSTR lpszPipename = TEXT("\\\\.\\pipe\\mynamedpipe");

	char message[255] = "";
	int n, i;
	printf("Introduceti numarul n: ");
	scanf_s("%d", &n);

	char* buffer = malloc(n * sizeof(char) + 1);

	for (i = 1; i < n; i++)
	{	
		_itoa(i, buffer, 10);
		strcat(message, buffer);
		strcat(message, ",");
	}

	_itoa(n, buffer, 10);
	strcat(message, buffer);
	printf("%s\n", message);

	lpvMessage =(LPCSTR)TEXT(message);

	if (argc > 1)
		lpvMessage = argv[1];
	// Incearca deschiderea pipe-ului cu nume;    
	// asteapta daca este necesar     
	while (1)
	{
		hPipe = CreateFile(
			lpszPipename,   // nume pipe          
			GENERIC_READ |  // acces read/write          
			GENERIC_WRITE,
			0,              // fara partajare          
			NULL,           // atribute de securitate implicite          
			OPEN_EXISTING,  // deschide un pipe existent          
			0,              // atribute implicite         
			NULL);          // fara fisier sablon/model (template)         
							// Break daca pipe-ul este invalid         
		if (hPipe != INVALID_HANDLE_VALUE)
			break;
		// Iesire daca apare o eroare alta decat ERROR_PIPE_BUSY         
		if (GetLastError() != ERROR_PIPE_BUSY)
		{
			printf("Nu pot deschide pipe!");
			return 0;
		}
		// Toate instantele pipe-ului sunt ocupate, asteptam 20 sec.          
		if (!WaitNamedPipe(lpszPipename, 20000))
		{
			printf("Timeout! Nu pot deschide pipe!");
			return 0;
		}
	}
	// Pipe-ul este conectat. Schimbare in mod de citire "mesaj"     
	dwMode = PIPE_READMODE_MESSAGE;
	fSuccess = SetNamedPipeHandleState(
		hPipe,    // handle pipe       
		&dwMode,  // noul mod pentru pipe      
		NULL,     // nu seteaza nr. maxim bytes        
		NULL);    // nu seteaza timp maxim     
	if (!fSuccess)
	{
		printf("Eroare la setarea pipe-ului!");
		return 0;
	}

	// Trimite un mesaj prin pipe server-ului      
	fSuccess = WriteFile(hPipe,                  // handle pipe       
		lpvMessage,             // mesajul       
		(lstrlen(lpvMessage) + 1) * sizeof(TCHAR), // lungime mesaj       
		&cbWritten,             // nr. de octeti transmisi efectiv       
		NULL);                  // I/O sincrona    
	if (!fSuccess)
	{
		printf("Eroare la transmitere mesaj!");
		return 0;
	}
	do
	{
		// Citire din pipe        
		fSuccess = ReadFile(
			hPipe,    // handle pipe           
			chBuf,    // buffer pentru receptie raspuns           
			BUFSIZE * sizeof(TCHAR),  // dimensiune buffer           
			&cbRead,  // nr. de octeti cititi           
			NULL);    // I/O sincrona   

		if (!fSuccess && GetLastError() != ERROR_MORE_DATA)
			break;

		printf(TEXT("%s\n"), chBuf);
	} while (!fSuccess);
	// repeta daca mai sunt date in pipe                                       
	// "eroarea" este ERROR_MORE_DATA 

	CloseHandle(hPipe);
	return 0;
}
